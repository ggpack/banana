export function randomNumber(min, max)
{
	return Math.floor(Math.random() * (max - min + 1) + min)
}

export function randomElem(...list)
{
	return list[randomNumber(0, list.length - 1)]
}

const randomByte = () => randomNumber(0, 255)
const randomPercent = () => (randomNumber(10, 50) * 0.01).toFixed(2)

export function randomCssRgba()
{
	return `rgba(${[randomByte(), randomByte(), randomByte(), randomPercent()].join(',')})`
}
