import React from "react"

import Draggable from "react-draggable"

import vinyl1 from "../images/vinyl1.png"
import vinyl2 from "../images/vinyl2.png"
import vinyl3 from "../images/vinyl3.png"
import vinyl4 from "../images/vinyl4.png"

import {Point} from "../utils/Point"
import {randomElem} from "../utils/Randoms"

const basename = str => str ? str.substring(str.lastIndexOf("/") + 1) : ""

const getSourceName = src => basename(new URL(src).pathname)

function useClickObserver(callback)
{
	const [dragStartPos, setDragStartPos] = React.useState(new Point())
	const onStart = (_, data) =>
	{
		setDragStartPos(Point.fromObj(data))
	}
	const onStop = (_, data) =>
	{
		const dragStopPoint = Point.fromObj(data)
		if(Point.dist(dragStartPos, dragStopPoint) < 5)
		{
			callback()
		}
	}
	return {onStart, onStop}
}

export default React.forwardRef(({pos, src, onPosChange}, ref) =>
{
	const nodeRef = React.useRef(null) // To solve strict-mode warning https://github.com/react-grid-layout/react-draggable/pull/478

	const [playing, setPlaying] = React.useState(false) // Just here to retrigger the refresh (css update)

	// Lazy init to do after the first user interaction
	const [audioState, setAudioState] = React.useState({setVolume: () => {}})

	const initAudio = React.useCallback(() =>
	{
		const context = new AudioContext()
		const audio = new Audio()

		audio.crossOrigin = "anonymous" // Useful to play hosted live stream with CORS enabled

		console.log("GGG audio", audio)

		const sourceAudio = context.createMediaElementSource(audio)
		const gainNode = context.createGain()
		const panner = context.createStereoPanner()

		sourceAudio.connect(gainNode).connect(panner).connect(context.destination)

		const errorHandler = e => {
			console.error('GG Error', e)
			audio.removeEventListener('error', errorHandler)
		}

		audio.onplaying = () => setPlaying(true)
		audio.onpause = () => setPlaying(false)

		//audio.addEventListener('canplaythrough', playHandler, false)
		audio.addEventListener('error', errorHandler)
		audio.src = src // Must be done after the CORS setup

		// First interaction will start the play
		context.resume()
		audio.play()

		const togglePlay = () =>
		{
			console.log('GG togglePlay', "paused?", audio.paused, context.state)
			audio.paused ? audio.play() : audio.pause()
		}

		const setVolume = (vol, pan) =>
		{
			gainNode.gain.value = vol
			panner.pan.value = pan
		}

		setAudioState({
			context,
			audio,
			togglePlay,
			setVolume,
		})
	}, [])

	const imgSrc =  React.useMemo(() => randomElem(vinyl1, vinyl2, vinyl3, vinyl4), [])

	// Connect the local setVolume to be called externally on the emitter's ref,
	// Also returns the node, to expose its position.
	React.useImperativeHandle(ref, () => ({setVolume: audioState.setVolume, elem: nodeRef.current}))

	const handleDrag = (event, data) =>
	{
		const newPos = Point.fromItem(data.node)
		onPosChange(newPos)
	}

	return (
		<Draggable
			bounds="parent"
			onDrag={handleDrag}
			defaultPosition={pos}
			{...useClickObserver(audioState?.togglePlay || initAudio)}
			nodeRef={nodeRef}
		>
			<div ref={nodeRef} className="DraggableBloc">
				<div ref={ref} className="ActorBloc Emitting" style={{
					backgroundColor: pos.color,
					animationPlayState: playing ? "running" : "paused"
				}}>
					<img src={imgSrc} className="ActorLogo" alt=""/>
				</div>
			</div>
		</Draggable>
	)
})
