import React from "react"

import Draggable from "react-draggable"

import listen from "../images/listen.png"
import {Point} from "../utils/Point"


export default function Listener({pos, onPosChange, posDelta, setPosDelta})
{
	const nodeRef = React.useRef(null) // To solve strict-mode warning https://github.com/react-grid-layout/react-draggable/pull/478
	const logoStyle = { transform: posDelta.rot() }

	const handleDrag = (_, data) =>
	{
		// data.x, y is buggy and translated to the bottom by the height of the elem x number of other draggables...
		// use the html item instead
		const newPos = Point.fromItem(data.node)
		onPosChange(newPos)

		const dragDelta = Point.fromField(data, "delta")
		// Do a weighted rolling average to avoid fast rotations.
		setPosDelta(posDelta.mult(4).plus(dragDelta).mult(0.2))
	}

	return (
		<Draggable
			bounds="parent"
			defaultPosition={pos}
			onDrag={handleDrag}
			nodeRef={nodeRef}
		>
			<div className="ActorBloc" style={{ backgroundColor: pos.color }} id="listener" ref={nodeRef}>
				<img src={listen} className="ActorLogo" alt="" style={logoStyle} />
			</div>
		</Draggable>
	)
}
