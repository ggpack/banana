#!/usr/bin/env bash
sourceImage=logo.png
destiDir=../../public/assets

# From a base icon of size 512x512
dim(){ echo ${size}x${size}; }

# Generates the assets in public folder for PWA support
for size in 64 192 512; do
	convert -resize $(dim) $sourceImage $destiDir/logo$(dim).png
done

# The 'maskable' icon
size=512
innerSize=$((size*75/100))
innerDims=${innerSize}x${innerSize}

convert -resize $innerDims -extent $(dim) -gravity center -background "#fafa44" \
	$sourceImage \
	$destiDir/logo$(dim)_maskable.png

# For Crapple, they are this kind of special...
size=192
convert -resize $(dim) -background "#fafa44" -flatten \
	$sourceImage \
	$destiDir/logo$(dim)_crapple.png
