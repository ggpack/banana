import React from "react"
import ReactDOM from "react-dom"

import "./index.css"
import App from "./App"
import * as serviceWorkerRegistration from "./serviceWorkerRegistration"

//https://felixgerschau.com/create-a-pwa-update-notification-with-create-react-app/
const serviceWorkerConfig =
{
	onSuccess: registration =>
	{
		console.log("service worker onSuccess")
	},

	// When a new SW has been installed
	onUpdate: async registration =>
	{
		console.log("service worker onUpdate")
		await registration.unregister()
		registration.waiting.postMessage({type: "SKIP_WAITING"})
		window.location.reload()
	}
}

ReactDOM.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
	document.getElementById("root")
)

serviceWorkerRegistration.register(serviceWorkerConfig)

window.addEventListener('beforeinstallprompt', e => {
	console.log('beforeinstallprompt')
})
