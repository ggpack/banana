import React from "react"

import "./App.css"
import Emitter from "./components/Emitter"
import Listener from "./components/Listener"

import {Point} from "./utils/Point"
import {randomCssRgba} from "./utils/Randoms"
import logo from "./images/logo.svg"

const refDist = Math.max(window.innerWidth, window.innerHeight) * 0.75

function clamp(min, val, max)
{
	return Math.max(min, Math.min(val, max))
}

function listenerEarsPos(pos, angle)
{
	// radius = 50
	const rota = new Point(-Math.sin(angle), Math.cos(angle))
	const leftEar = rota.mult(50).plus(pos)
	const rightEar = rota.mult(-50).plus(pos)
	return [leftEar, rightEar]
}

function updateEmitterVolume(emitterRef, emitterPos, listenerPos, listenerAngle)
{
	const [leftEar, rightEar] = listenerEarsPos(listenerPos, listenerAngle)
	const leftDist = Point.dist(leftEar, emitterPos)
	const rightDist = Point.dist(rightEar, emitterPos)
	const dist = Point.dist(listenerPos, emitterPos)

	const newVol = clamp(0, 1 - dist / refDist, 1)

	const alpha = new Point(rightDist, leftDist).angle()
	const newPan = Math.cos(2 * alpha)

	emitterRef.setVolume(newVol, newPan)
}


export default function App()
{
	const emittersData = [
		{
			src: "https://uk7.internet-radio.com/proxy/ukvibes?mp=/stream",
			pos: {x: 290, y: 60, color: React.useMemo(randomCssRgba, [])},
			ref: React.useRef(null)
		},
		{
			src: "https://uk7.internet-radio.com/proxy/movedahouse?mp=/stream",
			pos: {x: 30, y: 270, color: React.useMemo(randomCssRgba, [])},
			ref: React.useRef(null)
		},
		{
			src: "https://uk1.internet-radio.com/proxy/thezone?mp=/stream",
			pos: {x: 250, y: 170, color: React.useMemo(randomCssRgba, [])},
			ref: React.useRef(null)
		},
	]

	// curr - prev, used for stereo angle and doppler effect (later)
	const [posDelta, setPosDelta] = React.useState(new Point(0,1,0))

	const onListenerPosChange = listenerPos =>
	{
		for(const data of emittersData)
		{
			updateEmitterVolume(data.ref.current, Point.fromItem(data.ref.current.elem), listenerPos, posDelta.angle())
		}
	}

	const onEmitterPosChange = emitterRef => emitterPos =>
	{
		const elem = document.getElementById("listener")
		const listenerPos = Point.fromItem(elem)

		updateEmitterVolume(emitterRef.current, emitterPos, listenerPos, posDelta.angle())
	}

	return (
		<>
			<img
				style={{
					width: "95%",
					height: "95%",
					position: "absolute",
					inset: 0,
					margin: "auto",
					opacity: "30%",
				}}

				src={logo}
				draggable="false"
				alt="background"
				onDoubleClick={() => alert(`Your version: ${process.env.REACT_APP_VERSION || "local"}\nCredits: internet-radio.com\nRadios: ${emittersData.map(e => e.src.split("?")[0].split("/").pop())}`)}
			/>
			{emittersData.map((data, idx) => <Emitter {...data} key={idx} onPosChange={onEmitterPosChange(data.ref)} />)}
			<Listener pos={{x: 0, y: -200, color: React.useMemo(randomCssRgba, [])}}
				onPosChange={onListenerPosChange}
				{...{posDelta, setPosDelta}} />
		</>
	)
}
