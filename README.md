# Banana - Virtual streaming place


## Step 1
Click on the emitters to activate their sound streaming.

## Step 2
Drag your avatar on the map and hear the nearby sound emitters, in stereo.

# Credits
- Audio streaming by [Internet Radio]("https://www.internet-radio.com")
- Icons made by [Freepik]("https://www.freepik.com") from [www.flaticon.com]("https://www.flaticon.com").
